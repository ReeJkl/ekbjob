﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EkbJob.Database.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Resumes",
                columns: table => new
                {
                    ResumeId = table.Column<Guid>(nullable: false),
                    NameAndAge = table.Column<string>(nullable: true),
                    Vacancy = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Price = table.Column<string>(nullable: true),
                    Practise = table.Column<string>(nullable: true),
                    Education = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resumes", x => x.ResumeId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Resumes");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EkbJob.Database.Migrations
{
    public partial class ParsedInResume : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Parsed",
                table: "Resumes",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Parsed",
                table: "Resumes");
        }
    }
}

﻿// <auto-generated />
using System;
using EkbJob.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace EkbJob.Database.Migrations
{
    [DbContext(typeof(JobDbContext))]
    partial class JobDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.1.1-rtm-30846")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("EkbJob.Database.Entities.Resume", b =>
                {
                    b.Property<Guid>("ResumeId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City");

                    b.Property<string>("Education");

                    b.Property<string>("NameAndAge");

                    b.Property<bool>("Parsed");

                    b.Property<string>("Practise");

                    b.Property<string>("Price");

                    b.Property<string>("Vacancy");

                    b.HasKey("ResumeId");

                    b.ToTable("Resumes");
                });
#pragma warning restore 612, 618
        }
    }
}

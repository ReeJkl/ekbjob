﻿using EkbJob.Database.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EkbJob.Database
{
    public class JobDbContext : DbContext
    {
        public DbSet<Resume> Resumes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("User ID=postgres;Password=321;Host=localhost;Port=5432;Database=EkbJob;Pooling=true;");
        }
    }
}

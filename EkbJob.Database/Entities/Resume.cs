﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EkbJob.Database.Entities
{
    public class Resume
    {
        [Key]
        public Guid ResumeId { get; set; }

        /// <summary>
        /// Имя и возраст
        /// </summary>
        public string NameAndAge { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        public string Vacancy { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        public string Price { get; set; }

        /// <summary>
        /// Опыт работы
        /// </summary>
        public string Practise { get; set; }

        /// <summary>
        ///  Образование
        /// </summary>
        public string Education { get; set; }

        /// <summary>
        /// Спарсенная информация
        /// </summary>
        public bool Parsed { get; set; }
    }
}

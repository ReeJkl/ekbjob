﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EkbJob.Application.Views
{
    public class VmResume
    {
        [Key]
        public Guid ResumeId { get; set; }

        /// <summary>
        /// Имя и возраст
        /// </summary>
        [Required]
        [Display(Name = "Имя и возраст")]
        public string NameAndAge { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        /// [Required]
        [Display(Name = "Должность")]
        public string Vacancy { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        [Display(Name = "Город")]
        public string City { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        [Display(Name = "Цена")]
        public string Price { get; set; }

        /// <summary>
        /// Опыт работы
        /// </summary>
        [Display(Name = "Опыт работы")]
        public string Practise { get; set; }

        /// <summary>
        ///  Образование
        /// </summary>
        [Display(Name = "Образование")]
        public string Education { get; set; }

        /// <summary>
        /// Спарсенная информация
        /// </summary>
        [Display(Name = "Спарсено?")]
        public bool Parsed { get; set; }
    }
}

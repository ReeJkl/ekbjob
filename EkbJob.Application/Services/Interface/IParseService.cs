﻿using EkbJob.Database.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EkbJob.Application.Services.Interface
{
    public interface IParseService
    {
        Task ParseAsync();
    }
}

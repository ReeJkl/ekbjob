﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EkbJob.Application.Services.Interface
{
    public interface IRepository<T>
    {
        void Add(object entity);
        void AddFromModel(object model);
        void AddRange(IEnumerable<object> entities);
        void UpdateFromModel(object model);
        void DeleteFromModel(object model);
        void DeleteAll();
        T Get(Guid id);
        IQueryable<T> GetAll();
    }
}

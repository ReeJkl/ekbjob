﻿using EkbJob.Application.Services.Interface;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EkbJob.Application.Services
{
    public static class ServiceExtension
    {
        public static void AddIoCServices(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IParseService, ParserZarplataRuService>();
        }
    }
}

﻿using AutoMapper;
using EkbJob.Application.Views;
using EkbJob.Database.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EkbJob.Application.Services
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Resume, VmResume>();
            CreateMap<VmResume, Resume>();
        }
    }
}

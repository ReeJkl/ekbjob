﻿using AutoMapper;
using EkbJob.Application.Services.Interface;
using EkbJob.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EkbJob.Application.Services
{
    public class Repository<T> : IRepository<T> where T : class
    {
        JobDbContext db;
        readonly IMapper mapper;

        public Repository(JobDbContext context, IMapper _mapper)
        {
            db = context;
            mapper = _mapper;
        }

        public T Get(Guid id)
        {
            return db.Find<T>(id);
        }

        public IQueryable<T> GetAll()
        {
            return db.Set<T>().AsQueryable();
        }

        public void AddFromModel(object model)
        {
            var entity = mapper.Map<T>(model);
            var keyValue = typeof(T).GetProperties()
                .FirstOrDefault(x => x.CustomAttributes
                .Any(z => z.AttributeType == typeof(KeyAttribute)));
            keyValue.SetValue(entity, Guid.NewGuid());
            db.Add(entity);
            db.SaveChanges();
        }

        public void Add(object entity)
        {
            var keyValue = typeof(T).GetProperties()
                .FirstOrDefault(x => x.CustomAttributes
                .Any(z => z.AttributeType == typeof(KeyAttribute)));
            keyValue.SetValue(entity, Guid.NewGuid());
            db.Add(entity);
            db.SaveChanges();
        }

        public void AddRange(IEnumerable<object> entities)
        {
            var keyValue = typeof(T).GetProperties()
                .FirstOrDefault(x => x.CustomAttributes
                .Any(z => z.AttributeType == typeof(KeyAttribute)));
            foreach(var entity in entities)
            {
                keyValue.SetValue(entity, Guid.NewGuid());
            }
            db.AddRange(entities);
            db.SaveChanges();
        }

        public void UpdateFromModel(object model)
        {
            var entity = mapper.Map<T>(model);
            db.Update(entity);
            db.SaveChanges();
        }

        public void DeleteFromModel(object model)
        {
            var entity = mapper.Map<T>(model);
            db.Remove(entity);
            db.SaveChanges();
        }

        public void DeleteAll()
        {
            foreach (var p in db.Set<T>())
            {
                db.Entry(p).State = EntityState.Deleted;
            }
            db.SaveChanges();
        }
    }
}

﻿using EkbJob.Application.Services.Interface;
using EkbJob.Database.Entities;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EkbJob.Application.Services
{
    public class ParserZarplataRuService : IParseService
    {
        IRepository<Resume> _repository;
        public ParserZarplataRuService(IRepository<Resume> repository)
        {
            _repository = repository;
        }
        public async Task ParseAsync()
        {
            var handler = new HttpClientHandler();
            var httpClient = new HttpClient(handler);

            var request = new HttpRequestMessage(HttpMethod.Get, "https://ekb.zarplata.ru/resume");

            var myresponse = await httpClient.SendAsync(request);

            var document = new HtmlDocument();

            var stringContent = await myresponse.Content.ReadAsStringAsync();

            document.LoadHtml(stringContent);

            var resumesHtml = document.DocumentNode.Descendants()
                .Where(x => x.HasClass("resume-item"))
                .Select(x => x.ChildNodes[1].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0]);
            var resumes = resumesHtml.Select(x => new Resume()
            {
                ResumeId = Guid.NewGuid(),
                NameAndAge = x.SelectSingleNode(".//div[@class='eleven wide column']/div[1]").InnerText,
                City = x.SelectSingleNode(".//div[@class='eleven wide column']/div[2]/div/span[2]").InnerText,
                Vacancy = x.SelectSingleNode(".//div[@class='eleven wide column']/h2").InnerText,
                Education = x.SelectSingleNode(".//div[@class='five wide column']/div/div[2]").InnerText,
                Practise = x.SelectSingleNode(".//div[@class='five wide column']/div/div[1]").InnerText,
                Price = x.SelectSingleNode(".//div[@class='five wide column']/div/strong").InnerText,
                Parsed = true
            });
            _repository.AddRange(resumes);
        }
    }
}

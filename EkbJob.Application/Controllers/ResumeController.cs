﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EkbJob.Application.Services.Interface;
using EkbJob.Application.Views;
using EkbJob.Database.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EkbJob.Application.Controllers
{
    public class ResumeController : Controller
    {
        readonly IMapper _mapper;
        IRepository<Resume> _repository;
        public ResumeController(IMapper mapper, IRepository<Resume> repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        // GET: Resume
        public ActionResult Index()
        {
            var list = _repository.GetAll();
            var models = list.Select(x => _mapper.Map<VmResume>(x)).ToList();
            return View(models);
        }

        // GET: Resume/Parse
        public async Task<ActionResult> Parse([FromServices]IParseService parserService)
        {
            _repository.DeleteAll();
            await parserService.ParseAsync();
            return RedirectToAction("Index");
        }
    }
}